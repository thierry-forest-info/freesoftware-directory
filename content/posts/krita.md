---
title: "Krita"
date: 2023-10-07
draft: false
author: "mvirajprabhu"
---
## Krita

**Krita** (/ˈkriːtə/ KREE-tə) is a free and open-source raster graphics editor designed primarily for digital art and 2D animation. The software runs on Windows, macOS, Linux, Android, and ChromeOS, and features an OpenGL-accelerated canvas, colour management support, an advanced brush engine, non-destructive layers and masks, group-based layer management, vector artwork support, and switchable customisation profiles.

