---
title: "gimp"
date: 2023-10-07
draft: false
---
## Introduction
GIMP is a free and open-source raster graphics editor used for image manipulation and image editing, free-form drawing, transcoding between different image file formats, and more specialized tasks. It is extensible by means of plugins, and scriptable. 
